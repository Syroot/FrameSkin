﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Syroot.FrameSkin
{
    /// <summary>
    /// Represents a <see cref="Form"/> handling the required window messages to allow custom sized, layouted and
    /// painted window frames.
    /// </summary>
    public class CustomFrameForm : Form
    {
        // ---- MEMBERS ------------------------------------------------------------------------------------------------

        private Rectangle _windowRectangle;

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the frame has to be tested for a specific location at a given point.
        /// </summary>
        public event EventHandler<HitTestFrameEventArgs> HitTestFrame;

        /// <summary>
        /// Occurs when the frame has to be painted.
        /// </summary>
        public event EventHandler<PaintFrameEventArgs> PaintFrame;

        /// <summary>
        /// Occurs when the frame size has to be provided.
        /// </summary>
        public event EventHandler<ResizeFrameEventArgs> ResizeFrame;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Raises the <see cref="HandleCreated "/> event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected override void OnHandleCreated(EventArgs e)
        {
            if (DesignMode)
            {
                base.OnHandleCreated(e);
                return;
            }

            // Parts of this could be done in the constructor, but DesignMode is not set up then, so we do it here.

            // Required, since WM_ERASEBKGND should not be handled anymore, as it does not work correctly.
            // UserPaint is optional, but removes legwork not needed anymore for custom skins in the base Form class.
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            // Disable process-wide ghosting of window frames when the window is frozen, to prevent Windows from
            // redrawing the frame in system style, which causes ugly artifacts with custom frame drawing.
            Native.DisableProcessWindowsGhosting();

            // Disable visual style theming starting with Windows XP, since we want our own style.
            // This forces the window to be drawn in classic style, even on Aero. This makes the resizing sliver
            // overhang on adjacent monitors if the window is maximized in a multi-monitor environment. To manually fix
            // this like Windows XP and later did in the classic style, we set a window clipping region if the window
            // goes maximized (see OnWmSize). See https://blogs.msdn.microsoft.com/oldnewthing/20120326-00/?p=8003/.
            if (Environment.OSVersion.Version.Major >= 5 && Environment.OSVersion.Version.Minor >= 1)
            {
                // Disable visual styles on the frame.
                Native.SetWindowTheme(Handle, string.Empty, string.Empty);
            }

            DisableWindowDwmComposition();

            base.OnHandleCreated(e);
        }

        /// <summary>
        /// Processes Windows messages.
        /// </summary>
        /// <param name="m">The Windows <see cref="Message"/> to process</param>
        protected override void WndProc(ref Message m)
        {
            if (DesignMode)
            {
                base.WndProc(ref m);
                return;
            }

            switch ((Native.WindowMessage)m.Msg)
            {
                case Native.WindowMessage.Size:
                    OnWmSize(ref m);
                    break;
                case Native.WindowMessage.SetText:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.EraseBkgnd:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.NcCalcSize:
                    OnWmNcCalcSize(ref m);
                    break;
                case Native.WindowMessage.NcHitTest:
                    OnWmNcHitTest(ref m);
                    break;
                case Native.WindowMessage.NcPaint:
                    OnWmNcPaint(ref m);
                    break;
                case Native.WindowMessage.NcActivate:
                    OnWmNcActivate(ref m);
                    break;
                case Native.WindowMessage.NcMouseMove:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.NcLButtonDown:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.NcUahDrawCaption:
                    // Do nothing. This undocumented message handles visual style theming, which we do not want.
                    break;
                case Native.WindowMessage.NcUahDrawFrame:
                    // Do nothing. This undocumented message handles visual style theming, which we do not want.
                    break;
                case Native.WindowMessage.SysCommand:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.NcMouseLeave:
                    base.WndProc(ref m);
                    break;
                case Native.WindowMessage.DwmCompositionChanged:
                    OnWmDwmCompositionChanged(ref m);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static Padding PaddingFromRectangles(Rectangle outerRectangle, Rectangle innerRectangle)
        {
            return new Padding(innerRectangle.X - outerRectangle.X, innerRectangle.Y - outerRectangle.Y,
                outerRectangle.Right - innerRectangle.Right, outerRectangle.Bottom - innerRectangle.Bottom);
        }

        private static Rectangle PadRectangle(Rectangle rectangle, Padding padding)
        {
            return new Rectangle(rectangle.X + padding.Left, rectangle.Y + padding.Top,
                rectangle.Width - padding.Left - padding.Right, rectangle.Height - padding.Top - padding.Bottom);
        }

        private void OnWmSize(ref Message m)
        {
            // WParam contains the type of resizing requested.
            switch ((Native.SizeType)m.WParam)
            {
                case Native.SizeType.Maximized:
                    // Set clipping to remove overhanging resizing slivers on adjacent monitors.
                    Screen controlScreen = Screen.FromControl(this);
                    Rectangle workingArea = controlScreen.WorkingArea;
                    Debug.WriteLine(workingArea);
                    // Since the clip is relative to the window itself (which overhangs), the location has to be fixed.
                    if (RightToLeftLayout && RightToLeft == RightToLeft.Yes)
                    {
                        workingArea.Location = new Point(Location.X - workingArea.X - 1, -Location.Y + workingArea.Y);
                    }
                    else
                    {
                        workingArea.Location = new Point(-Location.X + workingArea.X, -Location.Y + workingArea.Y);
                    }
                    // Setting a region causes another WM_NCCALCSIZE and thus ResizeFrame with the new FormWindowState.
                    Region = new Region(workingArea);
                    break;
                case Native.SizeType.Restored:
                    // Remove any clipping if the window is not maximized.
                    // TODO: Do not get rid of clipping set by the user.
                    Region = null;
                    break;
            }

            // LParam specifies the new width and height of the client area, which we do not need here.

            // Call the base handling to let the window command take action.
            base.WndProc(ref m);
        }

        private void OnWmNcCalcSize(ref Message m)
        {
            if (m.WParam == (IntPtr)Native.FALSE)
            {
                // See https://blogs.msdn.microsoft.com/oldnewthing/20030911-00/?p=42553.
                // If WParam is FALSE, a window rectangle is provided, and the client rectangle should be returned.
                // It just seems to happen once at window creation time.
                // This is not required to be handled, and custom frame sizes are specified when WParam is TRUE.
                _windowRectangle = Native.GetStructure<Native.RECT>(m.LParam);
                // Compute the default frame size which can be adjusted by the user.
                Padding frameSize = GetDefaultFrameSize(_windowRectangle);
                frameSize = RaiseResizeFrame(frameSize);
                // Compute and return the client rectangle.
                Native.RECT clientRect = PadRectangle(_windowRectangle, frameSize);
                Marshal.StructureToPtr(clientRect, m.LParam, false);
            }
            else
            {
                // See https://blogs.msdn.microsoft.com/oldnewthing/20030915-00/?p=42493.
                // If WParam is TRUE, a structure is provided holding the following 3 rectangles in parent coordinates:
                // Rgrc[0] = new window rectangle, Rgrc[1] = old window rectangle, Rgrc[2] = old client rectangle
                Native.NCCALCSIZE_PARAMS calcSizeParams = Native.GetStructure<Native.NCCALCSIZE_PARAMS>(m.LParam);
                _windowRectangle = calcSizeParams.Rgrc[0]; // Remember for WM_NCPAINT later on.
                // Compute the default frame size which can be adjusted by the user.
                Padding frameSize = GetDefaultFrameSize(_windowRectangle);
                frameSize = RaiseResizeFrame(frameSize);
                calcSizeParams.Rgrc[0] = PadRectangle(_windowRectangle, frameSize);
                // When returning 0 below, the structure is expected to be filled out like this in parent coordinates:
                // Rgrc[0] = new client rectangle
                // Returning other than 0 means Rgrc[1] and [2] provide draw copy source and destination, not needed.
                Marshal.StructureToPtr(calcSizeParams, m.LParam, false);
            }

            // Always return 0 (required when WParam is FALSE and not used here when WParam is TRUE).
            m.Result = IntPtr.Zero;

            // Do not call the base implementation, as it would add the default window frame padding.
        }

        private void OnWmNcHitTest(ref Message m)
        {
            // WParam is not used.

            // LParam consists of the screen cursor X and Y coordinate in the lower and upper word.
            Point point = PointToClient(new Point(m.LParam.ToInt32()));

            // Raise an event to let the implementor return a hit test result.
            m.Result = new IntPtr((int)RaiseHitTestFrame(point));

            // Do not call the base implementation, as it would make Windows draw the default caption buttons.
        }

        private void OnWmNcPaint(ref Message m)
        {
            // TODO: Known bug: If RTL, a maximized window can be undocked at the top left corner since Windows 7. If
            // this is done, the window frame is not redrawn at all anymore, until a resize at the right or bottom
            // happens. Also happens when undocking on the right, but fixes itself after any resize.

            // Get the graphics context, clipped to the whole window rather than only the client area, and clip it to
            // the visible part of the window frame.

            // WParam contains the region to update, or 1 if the whole window has to be updated.
            IntPtr deviceContext = Native.GetDCEx(m.HWnd, m.WParam,
                Native.DeviceContextValues.Window | Native.DeviceContextValues.IntersectRgn
                | Native.DeviceContextValues.Cache | Native.DeviceContextValues.ClipSiblings);
            // Since Vista, GetDCEx returns NULL when whole window has to be repainted or client area is of empty size.
            if (deviceContext == IntPtr.Zero)
            {
                // Use the window graphics context as a workaround.
                deviceContext = Native.GetWindowDC(m.HWnd);
            }

            // Draw the frame, passing the window size.
            using (Graphics graphics = Graphics.FromHdc(deviceContext))
            {
                RaisePaintFrame(graphics, _windowRectangle);
            }

            Native.ReleaseDC(m.HWnd, deviceContext);

            // LParam is not used.

            // Return 0 if the message was processed.
            m.Result = IntPtr.Zero;

            // Do not call the base implementation, as it would still try to draw the system frame over it.
        }

        private void OnWmNcActivate(ref Message m)
        {
            // WParam contains the new state, TRUE for an active title bar, FALSE for an inactive.
            bool active = m.WParam != (IntPtr)Native.FALSE;

            // LParam is not used if a visual is style active, otherwise it is a handle to a region requiring a redraw.
            // If set to -1, DefWindowProc will not repaint the frame.
            m.LParam = new IntPtr(-1);

            // Return TRUE if we handled and drew the change.
            m.Result = (IntPtr)Native.TRUE;

            base.WndProc(ref m);
        }

        private void OnWmDwmCompositionChanged(ref Message m)
        {
            // Raised whenever DWM composition is turned on or off. WParam and LParam are not used.
            // This message should not be sent anymore starting with Windows 8, since DWM is always enabled there.
            // However, tempered systems re-enable the feature to turn off DWM in more or less well-implemented ways.
            DisableWindowDwmComposition();
        }

        private void DisableWindowDwmComposition()
        {
            // Prevent the DWM to render the frame (e.g. turn off "Aero") if enabled since Vista.
            // Even while SetWindowTheme already disables "Aero" on a frame, we make sure with this to keep it disabled
            // if the DWM gets turned on at a later time and might enable the frame.
            if (Environment.OSVersion.Version.Major >= 6)
            {
                // Check if DWM is enabled.
                bool dwmEnabled;
                Native.DwmIsCompositionEnabled(out dwmEnabled);

                // Turn off DWM handling of the frame.
                if (dwmEnabled)
                {
                    int renderingPolicy = (int)Native.DWMNCRENDERINGPOLICY.Disabled;
                    Native.DwmSetWindowAttribute(Handle, Native.DWMWINDOWATTRIBUTE.NCRenderingPolicy,
                        ref renderingPolicy, sizeof(int));
                }
            }
        }

        private Padding GetDefaultFrameSize(Rectangle windowRectangle)
        {
            // Get the default client rectangle to provide a computed default padding to the user.
            Native.RECT clientRect = windowRectangle;
            Native.AdjustWindowRectEx(ref clientRect,
                (uint)Native.GetWindowLong(Handle, (int)Native.GetWindowLongQuery.Style), false,
                (uint)Native.GetWindowLong(Handle, (int)Native.GetWindowLongQuery.ExStyle));
            // Compute the difference to provide a padding.
            return PaddingFromRectangles(clientRect, windowRectangle);
        }

        private HitTestResult RaiseHitTestFrame(Point cursorLocation)
        {
            if (HitTestFrame == null)
            {
                return HitTestResult.TitleBar;
            }
            else
            {
                HitTestFrameEventArgs e = new HitTestFrameEventArgs(cursorLocation);
                HitTestFrame(this, e);
                return e.Result;
            }
        }

        private void RaisePaintFrame(Graphics graphics, Rectangle windowRectangle)
        {
            if (PaintFrame == null)
            {
                graphics.Clear(Color.Black);
            }
            else
            {
                PaintFrameEventArgs e = new PaintFrameEventArgs(graphics, windowRectangle);
                PaintFrame(this, e);
            }
        }

        private Padding RaiseResizeFrame(Padding defaultFrameSize)
        {
            if (ResizeFrame == null)
            {
                return defaultFrameSize;
            }
            else
            {
                ResizeFrameEventArgs e = new ResizeFrameEventArgs(defaultFrameSize);
                ResizeFrame(this, e);
                return e.FrameSize;
            }
        }
    }
}
