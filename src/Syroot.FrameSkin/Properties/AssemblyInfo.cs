﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Syroot.FrameSkin")]
[assembly: AssemblyDescription("WinForms library to allow custom window border painting.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.FrameSkin")]
[assembly: AssemblyCopyright("WTFPL")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("4ed770c8-f170-48b6-afc4-f34bc96137a8")]
