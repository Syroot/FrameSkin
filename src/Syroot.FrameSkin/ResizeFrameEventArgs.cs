﻿using System;
using System.Windows.Forms;

namespace Syroot.FrameSkin
{
    public class ResizeFrameEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        internal ResizeFrameEventArgs(Padding frameSize)
        {
            FrameSize = frameSize;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the size of the window frame towards each direction. If not changed, this provides the default
        /// frame size. It is recommended to retain the left, right and bottom borders when a window is maximized or the
        /// borders might expand into the visible area (when too big) or let the client area run off-screen (when too
        /// small).
        /// </summary>
        public Padding FrameSize
        {
            get;
            set;
        }
    }
}