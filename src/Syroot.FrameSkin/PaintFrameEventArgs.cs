﻿using System;
using System.Drawing;

namespace Syroot.FrameSkin
{
    public class PaintFrameEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        internal PaintFrameEventArgs(Graphics gr, Rectangle windowRectangle)
        {
            Graphics = gr;
            WindowRectangle = windowRectangle;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="Graphics"/> instance with which drawing operations have to be performed.
        /// </summary>
        public Graphics Graphics
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the position and size of the window, relative to the parent (which is the screen for non-MDI windows).
        /// </summary>
        public Rectangle WindowRectangle
        {
            get;
            private set;
        }
    }
}