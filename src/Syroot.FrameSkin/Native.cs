﻿using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Syroot.FrameSkin
{
    internal static class Native
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal const int FALSE = 0;
        internal const int TRUE  = 1;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool AdjustWindowRectEx(ref RECT lpRect, uint dwStyle, bool bMenu, uint dwExStyle);

        [DllImport("user32.dll")]
        internal static extern void DisableProcessWindowsGhosting();

        [DllImport("user32.dll")]
        internal static extern IntPtr GetDCEx(IntPtr hWnd, IntPtr hrgnClip, DeviceContextValues flags);

        [DllImport("user32.dll")]
        internal static extern IntPtr GetWindowDC(IntPtr hWnd);

        internal static IntPtr GetWindowLong(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size == 4)
            {
                return GetWindowLong32(hWnd, nIndex);
            }
            return GetWindowLongPtr(hWnd, nIndex);
        }

        [DllImport("user32.dll")]
        internal static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);
        
        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        internal static extern int SetWindowTheme(IntPtr hWnd, string pszSubAppName, string pszSubIdList);

        [DllImport("dwmapi.dll")]
        internal static extern int DwmIsCompositionEnabled(out bool enabled);

        [DllImport("dwmapi.dll", PreserveSig = true)]
        internal static extern int DwmSetWindowAttribute(IntPtr hWnd, DWMWINDOWATTRIBUTE attr, ref int attrValue,
            int attrSize);

        internal static T GetStructure<T>(IntPtr pointer)
        {
            return (T)Marshal.PtrToStructure(pointer, typeof(T));
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport("user32.dll", EntryPoint = "GetWindowLong", SetLastError = true)]
        private static extern IntPtr GetWindowLong32(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr GetWindowLongPtr(IntPtr hWnd, int nIndex);

        // ---- ENUMERATIONS -------------------------------------------------------------------------------------------

        [Flags]
        internal enum DeviceContextValues
        {
            Window           = 0x00000001,
            Cache            = 0x00000002,
            NoResetAttrs     = 0x00000004,
            ClipChildren     = 0x00000008,
            ClipSiblings     = 0x00000010,
            ParentClip       = 0x00000020,
            ExcludeRgn       = 0x00000040,
            IntersectRgn     = 0x00000080,
            ExcludeUpdate    = 0x00000100,
            IntersectUpdate  = 0x00000200,
            LockWindowUpdate = 0x00000400,
            UseStyle         = 0x00010000,
            Validate         = 0x00200000,
        }

        internal enum DWMNCRENDERINGPOLICY
        {
            UseWindowStyle,
            Disabled,
            Enabled
        }

        internal enum DWMWINDOWATTRIBUTE
        {
            NCRenderingEnabled = 1,
            NCRenderingPolicy,
            TransitionsForceDisabled,
            AllowNCPaint,
            CaptionButtonBounds,
            NonClientRtlLayout,
            ForceIconicRepresentation,
            Flip3DPolicy,
            ExtendedFrameBounds,
            HasIconicBitmap,
            DisallowPeek,
            ExceludedFromPeek,
            Cloak,
            Cloaked,
            FreezeRepresentation
        }

        internal enum NonClientHitTestResult
        {
            Error       = -2,
            Transparent = -1,
            Nowhere     = 0,
            Client      = 1,
            Caption     = 2,
            SysMenu     = 3,
            GrowBox     = 4,
            Size        = 4,
            Menu        = 5,
            HScroll     = 6,
            VScroll     = 7,
            MinButton   = 8,
            Reduce      = 8,
            MaxButton   = 9,
            Zoom        = 9,
            Left        = 10,
            Right       = 11,
            Top         = 12,
            TopLeft     = 13,
            TopRight    = 14,
            Bottom      = 15,
            BottomLeft  = 16,
            BottomRight = 17,
            Border      = 18,
            Close       = 20,
            Help        = 21
        }

        [Flags]
        internal enum ExtendedWindowStyle : uint
        {
            LeftScrollbar       = 0x00000000,
            RightScrollbar      = 0x00000000,
            DialogModalFrame    = 0x00000001,
            NoParentNotify      = 0x00000004,
            TopMost             = 0x00000008,
            AcceptFiles         = 0x00000010,
            Transparent         = 0x00000020,
            MdiChild            = 0x00000040,
            ToolWindow          = 0x00000080,
            WindowEdge          = 0x00000100,
            ClientEdge          = 0x00000200,
            ContextHelp         = 0x00000400,
            Right               = 0x00001000,
            RtlReading          = 0x00002000,
            LtrReading          = 0x00004000,
            ControlParent       = 0x00010000,
            StaticEdge          = 0x00020000,
            AppWindow           = 0x00040000,
            Layered             = 0x00080000,
            NoInheritLayout     = 0x00100000,
            NoRedirectionBitmap = 0x00200000,
            LayoutRtl           = 0x00400000,
            Left                = 0x00400000,
            Composited          = 0x02000000,
            NoActivate          = 0x08000000,
            OverlappedWindow    = WindowEdge | ClientEdge,
            PaletteWindow       = WindowEdge | ToolWindow | TopMost
        }

        internal enum GetWindowLongQuery : int
        {
            UseRData   = -21,
            ExStyle    = -20,
            Style      = -16,
            ID         = -12,
            HwndParent = -8,
            HInstance  = -6,
            WndProc    = -4
        }

        internal enum SizeType
        {
            Restored  = 0,
            Minimized = 1,
            Maximized = 2,
            MaxShow   = 3,
            MaxHide   = 4
        }

        internal enum WindowMessage
        {
            Size                  = 0x0005,
            SetText               = 0x000C,
            EraseBkgnd            = 0x0014,
            Paint                 = 0x0015,
            NcCalcSize            = 0x0083,
            NcHitTest             = 0x0084,
            NcPaint               = 0x0085,
            NcActivate            = 0x0086,
            NcMouseMove           = 0x00A0,
            NcLButtonDown         = 0x00A1,
            NcUahDrawCaption      = 0x00AE,
            NcUahDrawFrame        = 0x00AF,
            SysCommand            = 0x0112,
            NcMouseLeave          = 0x02A2,
            DwmCompositionChanged = 0x031E
        }

        [Flags]
        internal enum WindowStyle : uint
        {
            Overlapped       = 0x00000000,
            Tiled            = 0x00000000,
            MaximizeBox      = 0x00010000,
            TabStop          = 0x00010000,
            Group            = 0x00020000,
            MinimizeBox      = 0x00020000,
            SizeBox          = 0x00040000,
            ThickFrame       = 0x00040000,
            SysMenu          = 0x00080000,
            HorizontalScroll = 0x00100000,
            VerticalScroll   = 0x00200000,
            DialogFrame      = 0x00400000,
            Border           = 0x00800000,
            Caption          = 0x00C00000,
            Maximize         = 0x01000000,
            ClipChildren     = 0x02000000,
            ClipSiblings     = 0x04000000,
            Disabled         = 0x08000000,
            Visible          = 0x10000000,
            Iconic           = 0x20000000,
            Minimize         = 0x20000000,
            Child            = 0x40000000,
            ChildWindow      = 0x40000000,
            Popup            = 0x80000000,
            PopupWindow      = Popup | Border | SysMenu,
            OverlappedWindow = Overlapped | Caption | SysMenu | ThickFrame | MinimizeBox | MaximizeBox,
            TiledWindow      = Tiled | Caption | SysMenu | ThickFrame | MinimizeBox | MaximizeBox
        }

        // ---- STRUCTURES ---------------------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        internal struct NCCALCSIZE_PARAMS
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            internal RECT[] Rgrc;
            internal WINDOWPOS Lppos;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;

            internal RECT(int left, int top, int right, int bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }

            internal RECT(Rectangle r)
                : this(r.Left, r.Top, r.Right, r.Bottom)
            {
            }

            internal int X
            {
                get { return Left; }
                set { Right -= (Left - value); Left = value; }
            }

            internal int Y
            {
                get { return Top; }
                set { Bottom -= (Top - value); Top = value; }
            }

            internal int Height
            {
                get { return Bottom - Top; }
                set { Bottom = value + Top; }
            }

            internal int Width
            {
                get { return Right - Left; }
                set { Right = value + Left; }
            }

            internal Point Location
            {
                get { return new Point(Left, Top); }
                set { X = value.X; Y = value.Y; }
            }

            internal Size Size
            {
                get { return new Size(Width, Height); }
                set { Width = value.Width; Height = value.Height; }
            }

            public static implicit operator Rectangle(RECT r)
            {
                return new Rectangle(r.Left, r.Top, r.Width, r.Height);
            }

            public static implicit operator RECT(Rectangle r)
            {
                return new RECT(r);
            }

            public static bool operator ==(RECT r1, RECT r2)
            {
                return r1.Equals(r2);
            }

            public static bool operator !=(RECT r1, RECT r2)
            {
                return !r1.Equals(r2);
            }

            public override bool Equals(object obj)
            {
                if (obj is RECT)
                {
                    return Equals((RECT)obj);
                }
                else if (obj is Rectangle)
                {
                    return Equals(new RECT((Rectangle)obj));
                }
                return false;
            }

            public override int GetHashCode()
            {
                return ((Rectangle)this).GetHashCode();
            }

            public override string ToString()
            {
                return string.Format(CultureInfo.CurrentCulture, "{{Left={0},Top={1},Right={2},Bottom={3}}}",
                    Left, Top, Right, Bottom);
            }

            internal bool Equals(RECT r)
            {
                return r.Left == Left && r.Top == Top && r.Right == Right && r.Bottom == Bottom;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct WINDOWPOS
        {
            internal IntPtr Hwnd;
            internal IntPtr HwndInsertAfter;
            internal int X;
            internal int Y;
            internal int CX;
            internal int CY;
            internal uint Flags;
        }
    }
}
