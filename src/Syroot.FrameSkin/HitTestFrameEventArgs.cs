﻿using System;
using System.Drawing;

namespace Syroot.FrameSkin
{
    public class HitTestFrameEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        internal HitTestFrameEventArgs(Point location)
        {
            Location = location;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets the location in client coordinates to which the underlying hit test <see cref="Result"/> should be
        /// returned.
        /// </summary>
        public Point Location
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the result of the hit test, determining the type of area under the <see cref="Location"/>.
        /// </summary>
        public HitTestResult Result
        {
            get;
            set;
        }
    }

    public enum HitTestResult
    {
        Error               = -2,
        Transparent         = -1,
        None                = 0,
        ClientArea          = 1,
        TitleBar            = 2,
        SystemMenu          = 3,
        GrowBox             = 4,
        HorizontalScrollbar = 6,
        VerticalScrollbar   = 7,
        MinimizeButton      = 8,
        MaximizeButton      = 9,
        BorderLeft          = 10,
        BorderRight         = 11,
        BorderTop           = 12,
        BorderTopLeft       = 13,
        BorderTopRight      = 14,
        BorderBottom        = 15,
        BorderBottomLeft    = 16,
        BorderBottomRight   = 17,
        BorderFixed         = 18,
        CloseButton         = 20,
        HelpButton          = 21
    }
}