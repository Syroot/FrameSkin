﻿namespace Syroot.FrameSkin.TestApplication
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btRightToLeft = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _btRightToLeft
            // 
            this._btRightToLeft.Location = new System.Drawing.Point(0, 0);
            this._btRightToLeft.Margin = new System.Windows.Forms.Padding(0);
            this._btRightToLeft.Name = "_btRightToLeft";
            this._btRightToLeft.Size = new System.Drawing.Size(122, 38);
            this._btRightToLeft.TabIndex = 0;
            this._btRightToLeft.Text = "Right To Left";
            this._btRightToLeft.UseVisualStyleBackColor = true;
            this._btRightToLeft.Click += new System.EventHandler(this._btRightToLeft_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this._btRightToLeft);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMain";
            this.Text = "Test Application";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btRightToLeft;
    }
}