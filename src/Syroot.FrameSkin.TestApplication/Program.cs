﻿using System;
using System.Windows.Forms;

namespace Syroot.FrameSkin.TestApplication
{
    /// <summary>
    /// The main class of the program containing the application entry point.
    /// </summary>
    internal static class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------
        
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}
