﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.FrameSkin.TestApplication
{
    public partial class FormMain : CustomFrameForm
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public FormMain()
        {
            InitializeComponent();

            //RightToLeft = RightToLeft.Yes;
            //RightToLeftLayout = true;

            //IsMdiContainer = true;
            //SkinnedForm bla = new SkinnedForm();
            //bla.RightToLeft = RightToLeft.Yes;
            //bla.RightToLeftLayout = true;
            //bla.MdiParent = this;
            //bla.Show();

            ResizeFrame += FormMain_ResizeFrame;
            PaintFrame += FormMain_PaintFrame;
        }
        
        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void FormMain_ResizeFrame(object sender, ResizeFrameEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                e.FrameSize = new Padding(e.FrameSize.Left, e.FrameSize.Top + 14, e.FrameSize.Right, e.FrameSize.Bottom);
            }
            else
            {
                e.FrameSize = new Padding(14, e.FrameSize.Top + 14, 14, 14);
            }
        }

        private void FormMain_PaintFrame(object sender, PaintFrameEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.BlueViolet, new Rectangle(Point.Empty, e.WindowRectangle.Size));
            e.Graphics.DrawRectangle(Pens.Yellow, new Rectangle(0, 0,
                e.WindowRectangle.Width - 1, e.WindowRectangle.Height - 1));
        }
        
        private void _btRightToLeft_Click(object sender, EventArgs e)
        {
            if (RightToLeftLayout)
            {
                RightToLeft = RightToLeft.No;
                RightToLeftLayout = false;
            }
            else
            {
                RightToLeft = RightToLeft.Yes;
                RightToLeftLayout = true;
            }
        }
    }
}
