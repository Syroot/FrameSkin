﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Syroot.FrameSkin.TestApplication")]
[assembly: AssemblyDescription("Test Application for Syroot.FrameSkin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.FrameSkin.TestApplication")]
[assembly: AssemblyCopyright("WTFPL")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("3c77e661-6bb0-40fd-86db-295d25d90801")]
