# FrameSkin
WinForms library to allow custom window border painting.

## Introduction

Drawing custom window borders is a complicated task, especially when having to deal with correct handling of P/Invoke calls in a managed .NET language to use it together with Windows Forms.

This library subclasses the default `System.Windows.Forms.Form`, adding properties, overridable methods and events to easily draw and design a custom window frame - fully supported under activated DWM (Aero) and up to Windows 10.

The project is under development and not yet finished, but if you are interested, you can already peek into the code!
